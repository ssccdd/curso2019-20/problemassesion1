/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo1;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public class Sesion1 {
    // Enumerado para el tipo de componente
    public enum TipoComponente {
        CPU(25), MEMORIA(50), PERIFERICO(100);
        
        private final int valor;

        private TipoComponente(int valor) {
            this.valor = valor;
        }
        
        /**
         * Obtenemos un componente relacionado con su valor de construcción
         * @param valor, entre 0 y 100, de constucción del componente
         * @return el TipoComponente con el valor de construcción
         */
        public static TipoComponente getComponente(int valor) {
            TipoComponente resultado = null;
            TipoComponente[] componentes = TipoComponente.values();
            int i = 0;
            
            while( (i < componentes.length) && (resultado == null) ) {
                if ( componentes[i].valor >= valor )
                    resultado = componentes[i];
                
                i++;
            }
            
            return resultado;
        } 
    }
    
    // Constantes del problema
    public static final int NUM_ORDENADORES = 10;
    public static final int VALOR_CONSTRUCCION = 101; // Valor máximo
    public static final int TOTAL_COMPONENTES = TipoComponente.values().length;
    public static final int COMPONENTES_A_CONSTRUIR = 20;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables aplicación
        Random aleatorio = new Random();
        Ordenador[] listaOrdenadores;
        Vendedor[] listaVendedores;
        TipoComponente[] listaComponentes = TipoComponente.values();
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos los vendedores y la lista de pedidos para losordenadores
        listaVendedores = new Vendedor[TOTAL_COMPONENTES];
        for( int i = 0; i < TOTAL_COMPONENTES; i++)
            listaVendedores[i] = new Vendedor(i+1,listaComponentes[i]);
        
        listaOrdenadores = new Ordenador[NUM_ORDENADORES];
        for( int i = 0; i < NUM_ORDENADORES; i++ ) {
            listaOrdenadores[i] = new Ordenador();
            listaOrdenadores[i].setiD(i+1);
        }
        
        // Cuerpo principal de ejecución
        int i = 0;
        while( i < COMPONENTES_A_CONSTRUIR ) {
            int valor = aleatorio.nextInt(VALOR_CONSTRUCCION);
            Vendedor vendedor = listaVendedores[TipoComponente.getComponente(valor).ordinal()];
            int ordenadorSeleccionado = aleatorio.nextInt(NUM_ORDENADORES);
            if ( listaOrdenadores[ordenadorSeleccionado].addComponente(vendedor) ) {
                i++;
            } else {
                System.out.println("(HILO_PRINCIPAL) El ordenador ya tiene ese "
                        + "componente " + vendedor);
            }
        }
        
        // Mostrar la información de la lista de ordenadores
        System.out.println("(HILO_PRINCIPAL) Lista de pedidos de ordenadores");
        for( Ordenador ordenador : listaOrdenadores )
            System.out.println(ordenador);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
