/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo2;

import es.uja.ssccdd.curso1920.problemassesion1.grupo2.Sesion1.TipoCapacidadGrafica;

/**
 *
 * @author Conde Rodriguez, Francisco de Asis
 */
public class Fotograma {
    
    private int id;
    private TipoCapacidadGrafica necesidad;

    public Fotograma(int id, TipoCapacidadGrafica necesidad) {
        this.id = id;
        this.necesidad = necesidad;
    }

    @Override
    public String toString() {
        return "Fotograma{" + "id=" + id + ", necesidad=" + necesidad + '}';
    }

    // - Solo se han implementado los metodos estrictamente necesarios
    public TipoCapacidadGrafica getNecesidad() {
        return necesidad;
    }
}
