/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo2;

import java.util.Random;

/**
 *
 * @author Conde Rodriguez, Francisco de Asis
 */
public class Sesion1 {
    
    // - Se usan constantes para hacer mas sencillos los futuros cambios
    public static final int NUM_ORDENADORES = 10;
    public static final int NUM_FOTOGRAMAS = 20;
    // - Numero de valores que tiene definido el tipo enumerado. Se guarda en
    //   una constante para no tener que repetir TipoCapacidadGrafica.values().length;
    //   cada vez que se use y tener un codigo mas claro.
    public static final int NUM_CAPACIDADES = TipoCapacidadGrafica.values().length;

    // Enumerado para el tipo de necesidades de un fotograma / capacidades de
    // un ordenador
    public enum TipoCapacidadGrafica {
        BASICAS(10), MEDIAS(40), AVANZADAS(160);
        
        private final int valor;

        private TipoCapacidadGrafica(int valor) {
            this.valor = valor;
        }
        
        /**
         * Obtenemos una capacidad/necesiad grafica relacionada con su valor de 
         * construcción (es su valor asociado)
         * @param valor de constucción de la capacidad gráfica 10, 40, o 100
         * @return el TipoCapacidadGrafica con el valor de construcción
         */
        public static TipoCapacidadGrafica getCapacidad(int valor) {
            TipoCapacidadGrafica resultado = null;
            TipoCapacidadGrafica[] capacidades = TipoCapacidadGrafica.values();
            int i = 0;
            
            while( (i < capacidades.length) && (resultado == null) ) {
                if ( capacidades[i].valor >= valor )
                    resultado = capacidades[i];
                
                i++;
            }
            
            return resultado;
        } 

        @Override
        public String toString() {
            return "TipoCapacidadGrafica{" + this.name() + ", valor=" + valor + '}';
        }  
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // - Definimos las variables
        Ordenador[] ordenadores;        
        Random generador;
        
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // - Creamos el array de ordenadores
        
        generador = new Random();
        ordenadores = new Ordenador[NUM_ORDENADORES];
        for (int i=0; i<NUM_ORDENADORES; i++) {
            int indice = generador.nextInt(NUM_CAPACIDADES);
            Ordenador ordenador = new Ordenador(TipoCapacidadGrafica.values()[indice]);
            ordenadores[i] = ordenador;
        }
        
        // - Generamos los fotogramas y los asignamos a un ordenador compatible
        
        // - Para evitar que todos los fotogramas queden agrupados en los primeros
        //   ordenadores, se tiene un indice que indica el siguiente ordenador
        //   al ultimo al que se le haya asignado fotograma para seguir a partir
        //   de ese. Inicialmente vale 0.
        int siguienteAComprobar = 0;
         
        for (int i=0; i<NUM_FOTOGRAMAS; i++) {
            // 1.- Generar el fotograma con necesidades aleatorias
            int idCapacidad = generador.nextInt(NUM_CAPACIDADES);
            TipoCapacidadGrafica necesidad = TipoCapacidadGrafica.values()[idCapacidad];
            Fotograma fotograma = new Fotograma(i, necesidad);
            
            // 2.- Recorrer el array de ordenadores a partir del ultimo ordenador
            //     al que se le haya asignado un fotograma buscando el primero
            //     con capacidades iguales a las necesidades que tiene nuestro 
            //     fotograma
            boolean asignado = false;
            int j = siguienteAComprobar;
            while(!asignado && (j<siguienteAComprobar + NUM_FOTOGRAMAS)) {
                asignado = ordenadores[j%NUM_ORDENADORES].asignaFotograma(fotograma);
                j++;
            }
            siguienteAComprobar = j%NUM_ORDENADORES;
        }
        
        // - Imprimimos por pantalla como ha quedado la lista de ordenadores.
        //   La clase Ordenador tiene redefinido el metodo toString() asi que
        //   se puede imprimir directamente.
        for (int i=0; i<NUM_ORDENADORES; i++) {
            System.out.println(ordenadores[i]);
        }
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
