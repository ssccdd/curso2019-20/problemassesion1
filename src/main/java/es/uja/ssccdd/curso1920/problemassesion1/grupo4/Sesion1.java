/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo4;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public class Sesion1 {
    // Enumerado para el tipo de sensores
    public enum TipoSensor {
        MOVIMIENTO(25), SONIDO(50), TEMPERATURA(75), CALOR(100);
        
        private final int valor;

        private TipoSensor(int valor) {
            this.valor = valor;
        }
        
        /**
         * Obtenemos un sensor relacionado con su valor de creación
         * @param valor, 0 y 100, de creación del sensor
         * @return el TipoSensor con el valor de creación
         */
        public static TipoSensor getSensor(int valor) {
            TipoSensor resultado = null;
            TipoSensor[] sensores = TipoSensor.values();
            int i = 0;
            
            while( (i < sensores.length) && (resultado == null) ) {
                if ( sensores[i].valor >= valor )
                    resultado = sensores[i];
                
                i++;
            }
            
            return resultado;
        } 
    }

    // Constantes del problema
    public static final int NUM_CASAS = 10;
    public static final int VALOR_CONSTRUCCION = 101; // Valor máximo
    public static final int TIPOS_SENSORES = TipoSensor.values().length;
    public static final int SENSORES_A_CONSTRUIR = 20;
    public static final int MIN_HABITACIONES = 3;
    public static final int VARIACION = 3; // Diferencia en habitaciones

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables aplicación
        Random aleatorio = new Random();
        Casa[] listaCasas;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización lista casas
        listaCasas = new Casa[NUM_CASAS];
        for(int i = 0; i < NUM_CASAS; i++) {
            int habitaciones = aleatorio.nextInt(VARIACION) + MIN_HABITACIONES;
            listaCasas[i] = new Casa(i+1, habitaciones);
        }
        
        // Cuerpo principal de ejecución
        int i = 0;
        while ( i < SENSORES_A_CONSTRUIR ) {
            int valor = aleatorio.nextInt(VALOR_CONSTRUCCION);
            TipoSensor tipoSensor = TipoSensor.getSensor(valor);
            Sensor sensor = new Sensor(i,tipoSensor);
            int numCasa = aleatorio.nextInt(NUM_CASAS);
            if( listaCasas[numCasa].addSensor(sensor) ) {
                i++;
            } else {
                System.out.println("(HILO_PRINCIPAL) La casa no tiene "
                        + "habitaciones para el" + sensor);

            }
        }
        
        // Mostrar la información de la lista de casas
        System.out.println("(HILO_PRINCIPAL) Lista de casas y sus sensores");
        for( Casa casa : listaCasas )
            System.out.println(casa);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
