/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion1.grupo4.Sesion1.TIPOS_SENSORES;
import es.uja.ssccdd.curso1920.problemassesion1.grupo4.Sesion1.TipoSensor;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class Casa {
    private final int iD;
    private final Sensor[][] sensoresHabitacion;
    private final int numHabitaciones;

    public Casa(int iD, int numHabitaciones) {
        this.iD = iD;
        this.numHabitaciones = numHabitaciones;
                
        sensoresHabitacion = new Sensor[numHabitaciones][TIPOS_SENSORES];
        for(int i = 0; i < numHabitaciones; i++)
            for(int j = 0; j < TIPOS_SENSORES; j++)
                sensoresHabitacion[i][j] = null;
    }

    public int getiD() {
        return iD;
    }

    public int getNumHabitaciones() {
        return numHabitaciones;
    }

    
    public Sensor[][] getSensoresHabitacion() {
        return sensoresHabitacion;
    }
    
    /**
     * Añade un sensor a una habitación que no disponga de el
     * @param sensor que se añade a la habitación
     * @return true si el sensor se ha añadido false en otro caso
     */
    public boolean addSensor(Sensor sensor) {
        boolean asignado = false;
        
        TipoSensor tipoSensor = sensor.getSensor();
        int i = 0;
        while ( (i < numHabitaciones) && !asignado ) 
            if( sensoresHabitacion[i][tipoSensor.ordinal()] != null ) {
                i++;
            } else {
                sensoresHabitacion[i][tipoSensor.ordinal()] = sensor;
                asignado = true;
            }
                
        return asignado;
    }

    @Override
    public String toString() {
        String resultado = "Casa[" + iD + "]:\n";
        
        for(int i = 0; i < numHabitaciones; i++)
            resultado = resultado + "\t Habitacion[" + (i+1) + "]: " + 
                    Arrays.toString(sensoresHabitacion[i]) + "\n";
        
        return resultado;
    }
    
    
    
}
