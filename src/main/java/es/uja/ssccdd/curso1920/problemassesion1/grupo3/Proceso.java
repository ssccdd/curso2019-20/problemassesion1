/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo3;

import es.uja.ssccdd.curso1920.problemassesion1.grupo3.Sesion1.TipoProceso;

/**
 *
 * @author pedroj
 */
public class Proceso {
    private final int iD;
    private final TipoProceso proceso;

    public Proceso(int iD, TipoProceso proceso) {
        this.iD = iD;
        this.proceso = proceso;
    }

    public int getiD() {
        return iD;
    }

    public TipoProceso getProceso() {
        return proceso;
    }

    @Override
    public String toString() {
        return "Proceso{" + "iD=" + iD + ", proceso=" + proceso + '}';
    }
}
