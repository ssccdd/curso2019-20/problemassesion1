/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion1.grupo3.Sesion1.TIPOS_PROCESOS;
import es.uja.ssccdd.curso1920.problemassesion1.grupo3.Sesion1.TipoProceso;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class Ordenador {
    private final int iD;
    private final Proceso[][] listaProcesos;

    public Ordenador(int iD, int[] maxProcesos) {
        this.iD = iD;
        listaProcesos = new Proceso[TIPOS_PROCESOS][];
        for(int i = 0; i < TIPOS_PROCESOS; i++) {
            listaProcesos[i] = new Proceso[maxProcesos[i]];
            for(int j = 0; j < maxProcesos[i]; j++)
                listaProcesos[i][j] = null;
        }
    }

    public int getiD() {
        return iD;
    }

    public Proceso[][] getListaProcesos() {
        return listaProcesos;
    }
    
    /**
     * Asignamos un proceso al ordenador si hay capacidad para ello
     * @param proceso proceso a asignar
     * @return true si el proceso ha sido asignado false en otro caso
     */
    public boolean addProceso(Proceso proceso) {
        boolean asignado = false;
        
        int i = 0;
        TipoProceso tipoProceso = proceso.getProceso();
        while ( (i < listaProcesos[tipoProceso.ordinal()].length) && !asignado )
            if (listaProcesos[tipoProceso.ordinal()][i] != null) {
                i++;
            } else {
                listaProcesos[tipoProceso.ordinal()][i] = proceso;
                asignado = true;
            }
                            
        
        return asignado;
    }

    @Override
    public String toString() {
        TipoProceso[] tipoProceso = TipoProceso.values();
        String resultado = "Ordenador[" + iD + "]:\n";
        
        for(int i = 0; i < TIPOS_PROCESOS; i++) {
            resultado = resultado + "\t" + tipoProceso[i] + "[max= " + 
                    listaProcesos[i].length + "]: " +
                    Arrays.toString(listaProcesos[i]) + "\n";
        }
        
        return resultado;
    }
}
