/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo4;

import es.uja.ssccdd.curso1920.problemassesion1.grupo4.Sesion1.TipoSensor;

/**
 *
 * @author pedroj
 */
public class Sensor {
    private final int iD;
    private final TipoSensor sensor;

    public Sensor(int iD, TipoSensor sensor) {
        this.iD = iD;
        this.sensor = sensor;
    }

    public int getiD() {
        return iD;
    }

    public TipoSensor getSensor() {
        return sensor;
    }

    @Override
    public String toString() {
        return "Sensor{" + "iD=" + iD + ", sensor=" + sensor + '}';
    }
}
