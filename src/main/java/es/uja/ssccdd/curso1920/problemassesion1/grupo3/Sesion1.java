/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo3;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public class Sesion1 {
    // Enumerado para el tipo de proceso
    public enum TipoProceso {
        INTENSIVO(25), LIGERO(50), MIXTO(100);
        
        private final int valor;

        private TipoProceso(int valor) {
            this.valor = valor;
        }
        
        /**
         * Obtenemos un proceso relacionado con su valor de creación 
         * @param valor, entre 0 y 100, de creación del proceso
         * @return el TipoProceso con el valor de creación
         */
        public static TipoProceso getProceso(int valor) {
            TipoProceso resultado = null;
            TipoProceso[] procesos = TipoProceso.values();
            int i = 0;
            
            while( (i < procesos.length) && (resultado == null) ) {
                if ( procesos[i].valor >= valor )
                    resultado = procesos[i];
                
                i++;
            }
            
            return resultado;
        } 
    }
    
    // Constantes del problema
    public static final int NUM_ORDENADORES = 10;
    public static final int VALOR_CONSTRUCCION = 101; // Valor máximo
    public static final int TIPOS_PROCESOS = TipoProceso.values().length;
    public static final int NUM_PROCESOS = 20;
    public static final int MAX_PROCESOS = 5; // Maximo de procesos de cada tipo

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables aplicación
        Random aleatorio = new Random();
        Ordenador[] listaOrdenadores;

        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización lista ordenadores
        listaOrdenadores = new Ordenador[NUM_ORDENADORES];
        for(int i = 0; i < NUM_ORDENADORES; i++) {
            int[] maxProcesos = new int[TIPOS_PROCESOS];
            for(int j = 0; j < TIPOS_PROCESOS; j++)
                maxProcesos[j] = aleatorio.nextInt(MAX_PROCESOS) + 1;
            listaOrdenadores[i] = new Ordenador(i+1, maxProcesos);
        }
        
        // Cuerpo principal de ejecución
        int i = 0;
        while( i < NUM_PROCESOS ) {
            int valor = aleatorio.nextInt(VALOR_CONSTRUCCION);
            TipoProceso tipoProceso = TipoProceso.getProceso(valor);
            Proceso proceso = new Proceso(i,tipoProceso);
            int numOrdenador = aleatorio.nextInt(NUM_ORDENADORES); // elegimos un ordenador aleatorio
            if (  listaOrdenadores[numOrdenador].addProceso(proceso) ) {
                i++;
            } else {
                System.out.println("(HILO_PRINCIPAL) El ordenador no tiene capacidad para ese "
                        + proceso);
            }
        }
        
        // Mostrar la información de la lista de ordenadores
        System.out.println("(HILO_PRINCIPAL) Lista de ordenadores con su asignación");
        for( Ordenador ordenador : listaOrdenadores )
            System.out.println(ordenador);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
