/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion1.grupo1;

import es.uja.ssccdd.curso1920.problemassesion1.grupo1.Sesion1.TipoComponente;

/**
 *
 * @author pedroj
 */
public class Vendedor {
    private final int iD;
    private final TipoComponente componente;

    public Vendedor(int iD, TipoComponente componente) {
        this.iD = iD;
        this.componente = componente;
    }

    public int getiD() {
        return iD;
    }

    public TipoComponente getComponente() {
        return componente;
    }

    @Override
    public String toString() {
        return "Vandedor{" + "iD=" + iD + ", componente=" + componente + '}';
    }
}
