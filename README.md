[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 1

Problemas propuestos para la Sesión 1 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion1/-/blob/master/README.md#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion1/-/blob/master/README.md#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion1/-/blob/master/README.md#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion1/-/blob/master/README.md#grupo-4)

### Grupo 1

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   `Vendedor`: Identificará a un vendedor por medio de un número de indentificación, para salvaguardar su identidad real. También tendrá asociado un `TipoComponente` que fabrica. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
    
-   `Ordenador`: Debe tener un componente de cada uno de los elementos de  `TipoComponente` para considerar que el ordenador está completo. El constructor de la clase no tendrá parámetros e inicializará las variables de instancia a un valor por defecto conocido por el desarrollador.
	-   Hay que definir los métodos de acceso correspondientes.
	-   Definir el método `toString()` para mostrar los elementos del ordenador con los datos del vendedor de cada componente. Si le falta algún componente por asignar debe indicar que está incompleto.

-   `Hilo Principal`:
    -   Crear un vendedor para todos los tipos de componente disponibles.
	-   Generar una lista de pedidos para 10 ordenadores.
	-   Mientras no se hayan generado 20 componentes hacer:
		-   Generar un valor de construcción aleatorio entre 0 y 100.
		-   Seleccionar el vendedor correspondiente para el componente con el valor de construcción generado.
		-   Asignar el componente aleatoriamente a uno de los ordenadores que aún no tenga ese componente.
	-   Presentar los ordenadores de la lista de pedidos.

### Grupo 2

En una granja de rendering (*render farm*) se procesan fotogramas para una película de animación. Cada fotograma puede requerir capacidades gráficas **básicas**, **medias** o **avanzadas**. Los ordenadores de la render farm tienen hardware gráfico con capacidades **básicas**, **medias** o **avanzadas**. Se trata de asignar los distintos fotogramas a los distintos ordenadores según sus capacidades gráficas.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   `Fotograma`: tendrá un número de identificación del fotograma. También tendrá asociado una `CapacidadGráfica` que indicará sus necesidades de capacidad gráfica. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
    
-   `Ordenador`: Tendrá una capacidad gráfica concreta y sólo podrá acoger a fotogramas que tengan esas necesidades gráficas.
	-   Hay que definir los métodos de acceso correspondientes.
	-   Un método que permita asignar un fotograma al ordenador. No puede sobrepasar la capacidades gráficas del ordenador.
	-   Definir el método `toString()`. Debe dar una representación legible de los fotogramas que tiene asignados un ordenador.
    
-   `Hilo Principal`:
	-   Generar una lista de **N** ordenadores con capacidades gráficas aleatorias.
	-   Mientras no se hayan generado 20 fotogramas hacer:
		-   Generar un fotograma con necesidades gráficas aleatorias.
		-   Seleccionar un ordenador que pueda generar ese fotograma.
		-   Asignar el fotograma al ordenador seleccionado.
	-   Presentar los ordenadores de la lista con los fotogramas que tiene asignado cada uno de ellos.

### Grupo 3

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   `Proceso`: Identificará a un proceso por medio de un número de identificación, para diferenciarlo del resto de procesos. También tendrá asociado un `TipoProceso` que indicará su función principal. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
    
-   `Ordenador`: Tendrá una capacidad limitada a los tipos de procesos que puede tener asignado.
	-   Hay que definir los métodos de acceso correspondientes.
	-   Un método que permita asignar un tipo de proceso al ordenador. No puede sobrepasar la capacidad para un tipo de proceso.
	-   Definir el método toString(). Debe dar una representación legible de los procesos que tiene asignados un ordenador y el número de procesos máximos de cada tipo.

-   `Hilo Principal`:
	-   Generar una lista de 10 ordenadores con capacidades aleatorias para los tipos de procesos que se les pueden asignar.
	-   Mientras no se hayan generado 20 procesos hacer:
		-   Generar un proceso de tipo aleatorio.
		-   Seleccionar un ordenador que pueda albergar el proceso.
		-   Asignar el proceso al ordenador seleccionado.
	-   Presentar los ordenadores de la lista con los procesos que tiene asignados cada uno de ellos.

### Grupo 4

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   `Sensor`: Identificará a un sensor por medio de un número de identificación para diferenciarlos. También tendrá asociado un `TipoSensor` que indicará su función. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
    
-   `Casa`: Tendrá un número de habitaciones determinadas en su constructor.
	-   Hay que definir los métodos de acceso correspondientes.
	-   Un método que permita asignar un tipo de sensor a una habitación. No puede haber más de un sensor de un tipo por habitación.
	-   Definir el método `toString()`. Tiene que quedar claro las habitaciones que componen la casa y sensores que hay en cada habitación.
	
-   `Hilo Principal`:
	-   Generar una lista de 10 casas de entre 3 a 5 habitaciones cada una de ellas. Para cada casa se generará de forma aleatoria el número de habitaciones.
	-   Mientras no se hayan generado 20 sensores hacer:
		-   Generar un sensor de tipo aleatorio.
		-   Seleccionar una casa que pueda albergar el sensor.
		-   Asignar el sensor a la casa seleccionada.
	-   Presentar las casas presentes en la lista.

<!--stackedit_data:
eyJoaXN0b3J5IjpbNzcxMzYwNjU1LC03MjEyNzI0OCwxMjUyMD
czNzMzLC0xNTQ1MzgxMzcyLC04MTY1OTgyNDQsMzQxMDg2NDIw
XX0=
-->